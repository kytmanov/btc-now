#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from gi.repository import Gtk, GLib
from gi.repository import AppIndicator3

from Bitfinex import *

class BtcTray:
    def __init__(self):
        self.currency = 'USD'
        """ При инициализации класса """
        self.ind = AppIndicator3.Indicator.new("btc-now",
                                               "/usr/share/btc-now/media/icon.png",
                                               AppIndicator3.IndicatorCategory.OTHER)
        self.ind.set_status(AppIndicator3.IndicatorStatus.ACTIVE)
        self.menu = Gtk.Menu()

        """ Собираем выпадающее меню """
        last_data = self.add_menu_item(self.menu,
                                       active=False)

        menu_volume = self.add_menu_item(self.menu,
                                         active=False)

        last_refresh = self.add_menu_item(self.menu,
                                          active=False)

        separator = Gtk.SeparatorMenuItem()
        separator.show()
        self.menu.append(separator)

        self.add_menu_item(self.menu,
                           label="Update",
                           command=lambda x: self.set_label(last_data, last_refresh, menu_volume))

        self.add_menu_item(self.menu,
                           label="Quit",
                           command=lambda x: Gtk.main_quit())

        """ Подключаем меню к аплету """
        self.ind.set_menu(self.menu)

        """ Выводим цену """
        self.set_label(last_data, last_refresh, menu_volume)
        GLib.timeout_add_seconds(60, lambda: self.set_label(last_data, last_refresh, menu_volume))

    def add_menu_item(self, menu, command="", label="Loading...", active=True):
        """ Генерирует элемент меню """
        self.menu = menu
        self.item = Gtk.MenuItem(label)
        if command:
            self.item.connect("activate", command)
        self.item.set_sensitive(active)
        self.item.show()
        self.menu.append(self.item)
        return self.item

    def update_label(self, label):
        """ Устанавливаем label """
        self.ind.set_label(label, " ")

    def update_menu(self, label, item):
        """ Устанавливаем label в меню """
        item.set_label(label)

    def set_label(self, last_data, last_refresh, menu_volume):
        """ Собираем label и отправляем на вывод"""
        a = Bitfinex()
        a.get_data()
        label = str(a.curent_price)
        print(label)
        self.update_label(label)

        label = str(a.low_price) + "/" + str(a.high_price) + "    -Low/High"
        self.update_menu(label, last_data)

        label =  str(a.volume) + "   -24h volume"
        self.update_menu(label, menu_volume)

        label =  str(a.long_volume) + "/" + str(a.short_volume) + "   -Long/Short %"
        self.update_menu(label, last_refresh)
        print(label)
        return True

    def main(self):
        Gtk.main()
        return 0

if __name__ == "__main__":
    app = BtcTray()
    app.main()