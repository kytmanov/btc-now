# README #

GTK Indicator Applet that displays bitcoin price from Bitfinex.

## Display:
* Last price
* Daily high and low price
* Daily volume
* Long / Short Swap Ratios from bfxdata

## Requirements
* Python 3
* requests lib