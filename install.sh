#!/bin/bash

sudo mkdir /usr/share/btc-now
sudo mkdir /usr/share/btc-now/media

sudo cp * /usr/share/btc-now/
sudo cp media/* /usr/share/btc-now/media/

cp btc-now.desktop ~/.config/autostart/
