import requests
import datetime
import math

class Bitfinex():
    def __init__(self):
        self.url = "https://api.bitfinex.com/v1/pubticker/btcusd"
        self.url_bfx = "http://bfxdata.com/json/longVsShortRatio1h.json"
        self.update_time = 0
        self.curent_price = 0
        self.low_price = 0
        self.high_price = 0
        self.volume = 0
        self.long_volume = 0
        self.short_volume = 0

    def get_json(self):
        """ Get data from server """
        try:
            self.json = requests.get(self.url).json()
            return self.json
        except:
            return 0

    def get_json_bfx(self):
        """ Get BFX data from server """
        try:
            self.json_bfx = requests.get(self.url_bfx).json()
            return self.json_bfx
        except:
            return 0

    def time_format(self, timestamp):
        """ Add time formate and """
        return datetime.datetime.fromtimestamp(float(timestamp)).strftime('%Y-%m-%d %H:%M:%S')

    def get_data(self):
        self.json = self.get_json()
        self.json_bfx = self.get_json_bfx()
        if self.json:
            self.update_time = self.time_format(self.json['timestamp'])
            self.curent_price = self.json['last_price']
            self.low_price = self.json['low']
            self.high_price = self.json['high']
            self.volume = round(float(self.json['volume']))
        if self.json_bfx:
            self.long_volume = math.trunc(float(self.json_bfx[0][1]) * 100)
            self.short_volume = math.trunc(float(self.json_bfx[1][1])* 100)

if __name__ == "__main__":
    a = Bitfinex()
    a.get_data()
    print(a.curent_price)
    print(a.long_volume, a.short_volume)